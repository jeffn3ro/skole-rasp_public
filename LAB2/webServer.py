from flask import Flask
import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN)
GPIO.setup(24, GPIO.IN)
GPIO.setup(18, GPIO.IN)

app = Flask(__name__)

@app.route('/')
def hello_world():
	return "Hello World" # returns to the web server

@app.route('/date/')
def datePage():
	return time.ctime()

@app.route('/switches/')
def switch():
	in_23=GPIO.input(23)
	in_24=GPIO.input(24)
	in_18=GPIO.input(18)
	return ("Switch 0: %s Switch 1: %s Switch 2: %s" % (in_23, in_24, in_18))

if __name__=='__main__':
	app.run(host='0.0.0.0') # every one is allowed
