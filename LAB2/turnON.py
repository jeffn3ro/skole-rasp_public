
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN)
GPIO.setup(17, GPIO.OUT)


print("Switch 0 (pin 18) is set as input")
print("LED 0 (pin 17) is set as output")
count = 0
oldValue = 1


while True:
        inputValue = GPIO.input(18)
	if (inputValue != oldValue):
	        if (inputValue == 1):
			ledOut = GPIO.output(17, GPIO.HIGH)
		else:
                	ledOut = GPIO.output(17, GPIO.LOW)
		print ("LED 0 is now " + str(inputValue))
		oldValue=inputValue
	time.sleep(.01)

