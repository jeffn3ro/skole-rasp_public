#!/bin/bash

#echo Exporting pin $1
#echo $1 > /sys/class/gpio/export
#echo Direction is out
#echo out > /sys/class/gpio/gpio$1/direction
if [ $2 = 1 ]
then
	echo $1 pin is set high.
else
	echo $1 pin is set low.

fi

echo $2 > /sys/class/gpio/gpio$1/value

