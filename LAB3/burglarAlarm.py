import RPi.GPIO as GPIO
import time
from SimpleCV import *

cam = Camera()
disp = Display()

lastImg = cam.getImage()
lastImg.show()

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN)
GPIO.setup(17, GPIO.OUT)


alarmOnOff = GPIO.input(23)

count = 0
oldValue = -1



while not disp.isDone():

	newImg = cam.getImage()
	trackImg = newImg - lastImg

	if (alarmOnOff == 1):
		blobs = trackImg.findBlobs(150)

		# Check for blob changes
		if blobs:
			GPIO.output(17, GPIO.HIGH)
			print("There is a thief here at %s" % (blobs) + '\n')
			time.sleep(5)
			GPIO.output(17, GPIO.LOW)
	else:
		print("Alarm OFF")


	newImg.save(disp)
	lastImg = newImg

