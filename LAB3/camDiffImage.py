from SimpleCV import *

disp=Display()
cam = Camera()
lastImg = cam.getImage()

while not disp.isDone():
	newImg = cam.getImage()
	trackImg = newImg - lastImg
	trackImg.save(disp)
	lastImg = newImg
