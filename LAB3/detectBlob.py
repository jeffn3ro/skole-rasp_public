from SimpleCV import *

cam = Camera() #setup the camera
disp=Display()

lastImg = cam.getImage()
lastImg.show()

while not disp.isDone():
	newImg = cam.getImage()
	trackImg = newImg - lastImg # diff the images
	blobs = trackImg.findBlobs(20) # use adapative blob detection
	now = int(time.time())

	#If blobs are found then motion has occured
	if blobs:
		print blobs
		newImg.save(disp)
	lastImg = newImg # update the image

