from SimpleCV import *
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)

ledsOn = "All LEDs on"

ledsOff = "All LEDs off"


disp = Display()
cam = Camera()

while not disp.isDone():

	img = cam.getImage()
	img.save(disp)
	barcodes = img.findBarcode()

	if barcodes is not None:
        	for barcode in barcodes:
			if (barcode.data == ledsOn):
               			print barcode.data
				GPIO.output(17, GPIO.HIGH)
				GPIO.output(22, GPIO.HIGH)
				GPIO.output(27, GPIO.HIGH)
			else:
				print barcode.data
                                GPIO.output(17, GPIO.LOW)
                                GPIO.output(22, GPIO.LOW)
                                GPIO.output(27, GPIO.LOW)


	if barcodes is None:
		print ("there is no barcode")


