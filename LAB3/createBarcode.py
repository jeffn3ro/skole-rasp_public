from elaphe import barcode
from SimpleCV import Image, Display

def get_qrCode (info):
	bcode=barcode('qrcode', info, \
	 	options=dict(version=1, eclevel='H'),\
	 	scale=2,margin=10, data_mode='8bits')
	return bcode


def get_ean13Code (info):

	bcode=barcode('ean13', info, \
 		options=dict(includetext=True),\
 		scale=2, margin=1)
	return bcode

disp=Display()
bcode=get_qrCode('123456789012')
img=Image(bcode)
img.save(disp)
img.save("test.jpg")
