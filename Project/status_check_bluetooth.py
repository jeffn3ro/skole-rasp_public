import sys
import binascii
import struct
import time
from bluepy.btle import UUID, Peripheral

custom_service_uuid = UUID(0xA000)
button_char_uuid    = UUID(0xA001)
led_char_uuid       = UUID(0xA002)

if len(sys.argv) != 2:
  print "Fatal, must pass device address:", sys.argv[0], "<device address="">"
  quit()

p = Peripheral(sys.argv[1], "random")
customService=p.getServiceByUUID(custom_service_uuid)

try:
    led = customService.getCharacteristics(led_char_uuid)[0]
    button = customService.getCharacteristics(button_char_uuid)[0]
    if (button.supportsRead() and led.supportsRead()):
	while 1:
		val1 = binascii.b2a_hex(button.read())
                val2 = binascii.b2a_hex(led.read())
		print ("button: 0x" + val1)
                time.sleep(0.5)
		led.write(struct.pack('<B', 0x01));
    		print ("Led2 on")
     		time.sleep(1)
      		led.write(struct.pack('<B', 0x00));
    		print ("Led2 off")
       		time.sleep(1)


finally:
    p.disconnect()






