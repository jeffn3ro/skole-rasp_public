#include "mbed.h"
#include "ble/BLE.h"
#include "CustomService.h"

DigitalOut statusLED(LED1, 0);
DigitalOut actuatedLED(LED2, 0);
InterruptIn button(BUTTON1);

const static char     DEVICE_NAME[] = "ButtonLED RPi11";
static const uint16_t uuid16_list[] = {CustomService::CUSTOM_SERVICE_UUID};
static CustomService *customServicePtr;

enum {
    RELEASED = 0,
    PRESSED,
    IDLE
};

static uint8_t buttonState = IDLE;

//Note that the buttonPressedCallback() executes in interrupt context, so it is safer to access *BLE device API from the main thread
void buttonPressedCallback(void)
{
    buttonState = PRESSED;
    actuatedLED = !actuatedLED;
}

//Note that the buttonReleasedCallback() executes in interrupt context, so it is safer to access *BLE device API from the main thread
void buttonReleasedCallback(void)
{
    buttonState = RELEASED;
}

// This callback allows the LEDService to receive updates to the ledState Characteristic. 
void onDataWrittenCallback(const GattWriteCallbackParams *params) {
    if ((params->handle == customServicePtr->getValueHandle()) && (params->len == 1)) {
        actuatedLED = *(params->data);
    }
}

// The function disconnectionCallback re-starts advertisements if connection is lost.
void disconnectionCallback(const Gap::DisconnectionCallbackParams_t *params)
{
    BLE::Instance().gap().startAdvertising();
}

void periodicCallback(void)
{
    statusLED = !statusLED; // turn alarm on if the LED is on
}

//This function is called when the ble initialization process has failled
void onBleInitError(BLE &ble, ble_error_t error)
{
    /* Initialization error handling should go here */
}

//Callback triggered when the ble initialization process has finished
void bleInitComplete(BLE::InitializationCompleteCallbackContext *params)
{ 
    BLE&        ble   = params->ble;
    ble_error_t error = params->error;
    if (error != BLE_ERROR_NONE) {
        /* In case of error, forward the error handling to onBleInitError */
        onBleInitError(ble, error);
        return;
    }
    
    /* Ensure that it is the default instance of BLE */
    if(ble.getInstanceID() != BLE::DEFAULT_INSTANCE) {
        return;
    }

    ble.gap().onDisconnection(disconnectionCallback);
    ble.gattServer().onDataWritten(onDataWrittenCallback);

    bool initialValueForLEDCharacteristic = false;
    /* Setup primary services */
    customServicePtr = new CustomService(ble, false, initialValueForLEDCharacteristic /* initial value for button pressed */);

    /* setup advertising */
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED | GapAdvertisingData::LE_GENERAL_DISCOVERABLE);

    //Make an AD strcture to to Send Advertising payload for the Custom service
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LIST_16BIT_SERVICE_IDS, (uint8_t *)uuid16_list, sizeof(uuid16_list));
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LOCAL_NAME, (uint8_t *)DEVICE_NAME, sizeof(DEVICE_NAME));
    
    ble.gap().setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);
    ble.gap().setAdvertisingInterval(1000); /* 1000ms. */
    ble.gap().startAdvertising();
}

int main(void)
{
    statusLED = 1;
    //Ticker executes a function at a specific rate
    Ticker ticker;
    button.fall(buttonPressedCallback);
    button.rise(buttonReleasedCallback);
    ticker.attach(periodicCallback, 1); /* Blink LED every second */
    //instantiate the bluetooth connection
    BLE &ble = BLE::Instance();
    ble.init(bleInitComplete);
    
    /* SpinWait for initialization to complete. This is necessary because the
     * BLE object is used in the main loop below. */
    while (ble.hasInitialized()  == false ) { /* spin loop */ }
    while (true) {
        if (buttonState != IDLE) {
            customServicePtr->updateButtonState(buttonState);
            customServicePtr->updateLedState(actuatedLED);
            buttonState = IDLE;
        }
        ble.waitForEvent();
    }
}