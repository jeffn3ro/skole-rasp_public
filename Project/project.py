import smbus
import time
import serial
import socket
import RPi.GPIO as GPIO
import sys
import binascii
import struct
from SimpleCV import*
from threading import Thread
from elaphe import barcode
from time import sleep
from flask import Flask,send_file,request
from bluepy.btle import UUID, Peripheral


GPIO.setmode(GPIO.BCM)

#Exteral/Internal control
GPIO.setup(18, GPIO.IN)
GPIO.setup(17, GPIO.OUT)


#Alarm ON/OFF setup
GPIO.setup(23, GPIO.IN)
GPIO.setup(27, GPIO.OUT)

# detection LED
GPIO.setup(22, GPIO.OUT)

#Camera Setup
cam = Camera()
lastImg = cam.getImage()

#Nunchuk and MicroController setup
port = "/dev/ttyACM0"
bus = smbus.SMBus(1) #opens /dev/i2c-1
address=0x52         #the Nunchuk I2C address
bus.write_byte_data(address,0x40,0x00)
bus.write_byte_data(address,0xF0,0x55)
bus.write_byte_data(address,0xFB,0x00)
SerialIO = serial.Serial(port,9600)
SerialIO.flushInput()



# Bluetooth thread
blue_led = "00"
blue_button = "00"
blue_state = "0"

# global Gui alarm and blob change via GUI
gui_alarm = 0
blob_val = 20


#UDP setup
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # User Datagram (UDP)
UDP_IP_SEND = "128.39.112.73"
UDP_IP_RECV = "0.0.0.0"
UDP_PORT = 9050
sock.bind((UDP_IP_RECV, UDP_PORT))
print "UDP target IP:",   UDP_IP_SEND
print "UDP target port:", UDP_PORT


#UDP recive in thread
def recvUDP(sock,SerialIO):
	while True:
		data, addr = sock.recvfrom(1280) # Max recieve size is 1280 bytes
		inputValue = GPIO.input(18)
                alarmOnOff = GPIO.input(23)

		data_arr = data.split(",")
		mbed = "$C,1,%s\r\n" % data_arr[1]
		global blob_val
		global gui_alarm
		gui_alarm = int(data_arr[2])
                blob_val =  int(data_arr[4])

                if(data_arr[3] ==  "1"):
                        img = cam.getImage()
                        img.save("test.jpg")

		if(inputValue == 0):
			SerialIO.write(mbed)

#web server in thread
def webS():
	app = Flask(__name__)
	@app.route('/')
	def SendImage():
                return send_file('test.jpg',mimetype='image/jpg')

	if __name__=='__main__':
		app.run(host=UDP_IP_RECV)

#bluetooth in thread
def blue_t():
	#Bluetooth Setup
	custom_service_uuid = UUID(0xA000)
	button_char_uuid    = UUID(0xA001)
	led_char_uuid       = UUID(0xA002)
	state_change = True
	oldVal_switch = 0
	oldVal_gui = 0
	gui_alarm
	if len(sys.argv) != 2:
        	print "Fatal, must pass device address:", sys.argv[0], "<device address="">"
        	quit()

	p = Peripheral(sys.argv[1], "random")
	customService=p.getServiceByUUID(custom_service_uuid)
	led = customService.getCharacteristics(led_char_uuid)[0]
	button = customService.getCharacteristics(button_char_uuid)[0]


	try:
		global blue_led
		global blue_button
		global blue_state
		while True:
		        alarmOnOff = GPIO.input(23)
                        blue_led = binascii.b2a_hex(led.read())
                        blue_button = binascii.b2a_hex(button.read())

	                if ((oldVal_switch != alarmOnOff or oldVal_gui != gui_alarm or blue_led == "01") and state_change == True):
				led.write(struct.pack('<B', 0x01));
                                GPIO.output(27, GPIO.HIGH)
	                        oldVal_switch = alarmOnOff
        	                oldVal_gui = gui_alarm
                	        state_change = False

               		elif((oldVal_switch != alarmOnOff or oldVal_gui != gui_alarm or blue_led == "00") and state_change == False):
	                        led.write(struct.pack('<B', 0x00));
                      		GPIO.output(27, GPIO.LOW)
                        	oldVal_switch = alarmOnOff
                        	oldVal_gui = gui_alarm
                        	state_change = True

	                if(blue_led == "01" and blue_button == "01"):
        	                blue_state = "1";
        	        elif(blue_led == "00" and blue_button == "01"):
                	        blue_state = "0";

	finally:
		p.disconnect()


#threadstart flask web server
t_web = Thread(target=webS)
t_web.daemon=True
t_web.start()

#Thread start udpRecv
t = Thread(target=recvUDP,args=(sock,SerialIO))
t.daemon=True
t.start()

#Thread start for bluetooth service
t = Thread(target=blue_t)
t.daemon=True
t.start()

while True:
	try:
		time.sleep(0.08)
		bus.write_byte(address,0x00)
		# I2C or WII nunchuk setup
		data0 =  bus.read_byte(address)
                data1 =  bus.read_byte(address)
                data2 =  bus.read_byte(address)
                data3 =  bus.read_byte(address)
                data4 =  bus.read_byte(address)
                data5 =  bus.read_byte(address)
                data = [data0, data1, data2, data3, data4, data5]
                joy_x = data[0]
                joy_y = data[1]
                z_push = data[5] & 0x01
                c_push = (data[5] & 0x02) >> 1
		#Check GPIO status
		inputValue = GPIO.input(18)
                alarmOnOff = GPIO.input(23)
		alarm_state = GPIO.input(27)
		#Value sent to mbed for stepper change
		str_sum_local = "$C,0,%s\r\n" % (joy_x)

                newImg = cam.getImage()

		# take a pic with z push button
		if (z_push == 0):
			img = cam.getImage()
                	img.save("test.jpg")

		# Check for blobs or pixel movements if the Alarm is on
		if(alarm_state == 1):
	                trackImg = newImg - lastImg
        	        blobs = trackImg.findBlobs(blob_val)
                        if blobs:
                                newImg.save("test.jpg")
                                GPIO.output(22, GPIO.HIGH)
                        else:
                                GPIO.output(22, GPIO.LOW)
			lastImg = newImg

########################### Recive UDP Messages ##########################
		# external / local stepper values
		if (inputValue == 0):
	 		GPIO.output(17, GPIO.LOW)
		else:
                        GPIO.output(17, GPIO.HIGH)
                        SerialIO.write(str_sum_local)


		# send messages to GUI if there is any.
########################### SEND UDP Messages ##########################
		if (SerialIO.inWaiting() > 0):
        		inputLine = SerialIO.readline().strip()  # read a '\n' terminated line()
			sock.sendto(inputLine + "," + str(inputValue) + "," + str(alarmOnOff) + "," + blue_state + "," + str(alarm_state) + ",",   (UDP_IP_SEND, UDP_PORT))
			print inputLine



	except IOError as e:
		print e
