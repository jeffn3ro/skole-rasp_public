﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Timers;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            GoingThroughChanges();


        }
        private bool changedStated = true;
        private string oldAlarmVal = "0";
        private string oldBlueVal = "0";
        private Thread workerThread;
        public string strRecievedUDPMessage;
        public void UdpDataRecieved(object sender, EventArgs e)
        { //Show the message in the form
            textBox2.Text = strRecievedUDPMessage;
        }
        
        bool buttonPressed = false;

        private void recivedText()
        {

            string[] comands = textBox2.Text.Split(','); // Make a string table of comands

            double cam_pos = Convert.ToDouble(comands[0], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            progressBar1.Value = (int)(cam_pos / 3.6);
            
            //toggle external/local
            if (comands[1] == "1") pictureBox2.Visible = false;
            else pictureBox2.Visible = true;
           
            //toggle alarm On/Off
            if (comands[4] == "1") pictureBox4.Visible = true;
            else pictureBox4.Visible = false;

            if (checkBox2.Checked != changedStated)
            {
                textBox4.Text = "GUI";//The top image showing toogleOn.png
                changedStated = checkBox2.Checked;
            }
            else if (comands[2] != oldAlarmVal)
            {
                textBox4.Text = "Switch";
                oldAlarmVal = comands[2];
            }
            else if (comands[3] != oldBlueVal)
            {
                textBox4.Text = "Bluetooth";
                oldBlueVal = comands[3];
            }


        }

        public void GoingThroughChanges()
        {
            string sendtUDP = "$VAL";
            if (trackBar1.Value == trackBar1.Value) sendtUDP += "," + trackBar1.Value.ToString();
            if (checkBox2.Checked == true) sendtUDP += ",1";
            else sendtUDP += ",0";

            if (buttonPressed)
            {
                sendtUDP += ",1";
                buttonPressed = false;
            }
            else sendtUDP += ",0";

            textBox1.Text = sendtUDP + "," + numericUpDown1.Value.ToString();
        }

        public void DoReciveUDP()
        {
            UdpClient sock = new UdpClient(9050);
            IPEndPoint iep = new IPEndPoint(IPAddress.Any, 0);
            while (true)
            {
                try
                { //Recieve message as UDP
                    byte[] data = sock.Receive(ref iep);
                    //Convert Bytes to a ASCII String
                    strRecievedUDPMessage =
                    Encoding.ASCII.GetString(data, 0, data.Length);
                    //Cal the function UdpDataRecieved
                    this.Invoke(new EventHandler(this.UdpDataRecieved));
                }
                catch (Exception e) { }
            }
            sock.Close();
        }

        private void TextBoxSendt()
        {
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("128.39.113.162"), 9050);
            byte[] data = new byte[1024];
            string sendeString = textBox1.Text.ToString();
            data = Encoding.ASCII.GetBytes(sendeString);
            server.SendTo(data, data.Length, SocketFlags.None, ipep);

}
        
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            recivedText();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            trackBar1.Minimum = 0;
            trackBar1.Maximum = 360;

            workerThread = new Thread(this.DoReciveUDP);
            workerThread.IsBackground = true; //End
            // Start the worker thread.
            workerThread.Start();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TextBoxSendt();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            GoingThroughChanges();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buttonPressed = true;
            GoingThroughChanges();

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            GoingThroughChanges();
            if (checkBox2.Checked == true) pictureBox4.Visible = true; //The top image showing toogleOn.png
            else pictureBox4.Visible = false;    
        }


        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            recivedText();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            GoingThroughChanges();
        }
    }
}
