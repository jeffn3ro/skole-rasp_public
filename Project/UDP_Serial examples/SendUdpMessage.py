import socket
import time

UDP_IP = "128.39.112.73"
UDP_PORT = 9050
print "UDP target IP:",   UDP_IP
print "UDP target port:", UDP_PORT

counter=0;

sock = socket.socket(socket.AF_INET,     # Internet protocol
                     socket.SOCK_DGRAM)  # User Datagram (UDP)
while True:

    #Make a csv string 
    Message= "Dette er en test"

    # Send the csv string as a UDP message
    sock.sendto(Message, (UDP_IP, UDP_PORT))

    print "Sendt Message: " + Message

    #Wait a 500ms before retansmitting data
    time.sleep(0.5)

