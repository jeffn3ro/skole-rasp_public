#include "mbed.h"
BusOut StepMotor(PA_9, PC_7, PB_6, PA_7);

int present;
char buffer[80];
int val;
int oldVal;
int nun_buttons[2];

void right_l() 
{
    if (present == 255) {
        StepMotor = 0x0;
    }
    else {    
        for(int i = 0; i < 2; i++) {
            StepMotor = 0x6; // 0110
            wait_ms(10);
            StepMotor = 0xC; // 1100
            wait_ms(10);
            StepMotor = 0x9; // 1001
            wait_ms(10);
            StepMotor = 0x3; // 0011
            wait_ms(10);
        }
        present++;
    }
}

void left_l() 
{
    if (present == 0) {
        StepMotor = 0x0;
    }
    else {
        for(int i = 0; i < 2; i++) {
            StepMotor = 0x9; // 1001
            wait_ms(10);
            StepMotor = 0xC; // 1100
            wait_ms(10);
            StepMotor = 0x6; // 0110
            wait_ms(10);
            StepMotor = 0x3; // 0011
            wait_ms(10);
        }
        present--;
    }
}


int main()
{
    present = 0;
    while (true) {       
        scanf("%[^\r\n]s",buffer);  // Read a whole line
        getchar();                  // remove "\r\n" from stio
        getchar();        
        int count = 0;   // counter
        char* tok;      //  holds the string element
        tok = strtok(buffer, ",");   // get the first element in string before the ","
        if (strcmp(tok,"$C")==0) {
            while((tok != NULL) && (count < 2)) {
                tok = strtok(NULL, ",");
                nun_buttons[count] = atoi(tok);      
                count++;
            }
        }
        
        if( nun_buttons[0] == 0) 
        {
            if( nun_buttons[1] <= 100) left_l();
            else if ( nun_buttons[1] >= 200) right_l();
            else StepMotor = 0x0;
            printf("%d\n", (present*360/256));
        } 
        
        else 
        {  
            val = nun_buttons[1] * 255/360;
        
            while (oldVal < val) 
            { 
                right_l();
                oldVal++;
            }
            while (oldVal > val)
            {     
                left_l();
                oldVal--;
            }                    
            printf("%d\n", nun_buttons[1]);            
        }
    }
}